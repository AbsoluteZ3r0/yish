#ifndef YSHHEADER_H_   /* Include guard */
#define YSHHEADER_H_

void handle_signal(int signo);
void fill_argv(char *tmp_argv);
void copy_envp(char **envp);
void get_path_string(char **tmp_envp, char *bin_path);
void insert_path_str_to_search(char *path_str);
int attach_path(char *cmd);
void call_execve(char *cmd);
void free_argv();


#endif
